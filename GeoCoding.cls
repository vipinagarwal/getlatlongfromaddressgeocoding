 /*
         * Method Name: Request
         * Purpose : get lat and long from address
         * Paramter : String addres
         * Return :   Map < String, Object > 
         */
    @AuraEnabled
    public static Map < String, Object > Request(String address) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        String api = Label.GoogleAPIKey;
        // Can only request a single address at a time 
        String endpoint = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address;
        endpoint += '&key=' + api;

        request.setEndpoint(endpoint.replace(' ', '+')); // Remove spaces in addresses 
        request.setMethod('GET');

        HttpResponse response = http.send(request);

        Map < String, Object > resultJSON = (Map < String, Object > ) JSON.deserializeUntyped(response.getBody());

        if ((String) resultJSON.get('status') != 'OK') {
            // Log/Handle error 
        } else {
            List < Object > apiResult = (List < Object > ) resultJSON.get('results');

            Object actualResult = (Object) apiResult[0]; // Gets the 'address_components' section from the result

            Map < String, Object > returnMap = new Map < String, Object > ();

            Map < String, Object > actualResultMap = (Map < String, Object > ) actualResult; // Cast to Map to work with
            Map < String, Object > geo = (Map < String, Object > ) actualResultMap.get('geometry'); // Move into geometry section- contains location data needed
            Map < String, Object > coords = (Map < String, Object > ) geo.get('location'); // Contains exact location information

            returnMap.put('lng', coords.get('lng'));
            returnMap.put('lat', coords.get('lat'));

            returnMap.put('formatted_address', actualResultMap.get('formatted_address'));

            return returnMap;
        }

        return null;
    }